<?php

use Illuminate\Database\Seeder;

class TaskSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tasksStatus = ['PENDING', 'ABORTED', 'REJECTED', 'DECLINED', 'RESOLVED', 'COMPLETED'];
        $task_userStatus = ['PENDING', 'RELINQUISHED', 'REJECTED ', 'DECLINED', 'RESOLVED', 'COMPLETED'];

        $usersArray = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw('SELECT * FROM users WHERE id IN(SELECT DISTINCT user_id FROM team_user)'));

        collect($usersArray)
            ->each(function ($user) use ($tasksStatus, $task_userStatus) {
                $user = \App\User::find($user->id);
                $faker = \Faker\Factory::create();

                for ($i = 0; $i < rand(28, 32); $i++) {
                    $randomTaskStatusIndex = rand(0, count($tasksStatus)-1);
                    $dateAssignedInMinutes = rand(0, 1440*90);
                    $dateDeadlineInMinutes = $dateAssignedInMinutes+rand(1440*2, 1440*10);
                    $dateAssigned = \Carbon\Carbon::now()->addMinute($dateAssignedInMinutes);
                    $dateDeadline = \Carbon\Carbon::now()->addMinute($dateDeadlineInMinutes);
                    $dateCompleted = \Carbon\Carbon::now()->addMinute(rand(($dateAssignedInMinutes+$dateDeadlineInMinutes)/2.5 , $dateDeadlineInMinutes));

                    $task = $user->tasks()->save(factory(\App\Task::class)
                        ->make([
                            'status' => $tasksStatus[$randomTaskStatusIndex],
                            'response' => $randomTaskStatusIndex >= 4 ? $faker->paragraphs(rand(0, 3), true) : NULL,
                            'team_id' => $user->team[0]->id,
                            'created_by' => $user->team[0]->leader[0]->id,
                            'deadline_at' => $dateDeadline,
                            'created_at' => $dateAssigned,
                            'updated_at' => $dateCompleted
                        ]),
                        [
                            'status' => $task_userStatus[$randomTaskStatusIndex],
                            'completed_at' => $randomTaskStatusIndex >= 4 ? $dateCompleted : NULL,
                        ]
                    );
                }
            });
    }
}
