<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'himanshuthakkar@gmail.com')->get()->first();
        if(!$user){
            User::create([
                'name'=>'Himanshu Thakkar',
                    'email'=>'himanshuthakkar@gmail.com',
                'password'=>Hash::make('12345678'),
                'role'=>'manager'
            ]);
        } else {
            $user->update(['role'=>'manager']);
        }

        User::create([
            'name'=>'Dhruvin Thakkar',
            'email'=>'dhruvin@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Shubh Kothari',
            'email'=>'shubh@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Aryan Dhami',
            'email'=>'aryan@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Jay Rambhiya',
            'email'=>'jay@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Ram Poptani',
            'email'=>'ram@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Bhavesh Galani',
            'email'=>'bhavesh@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Chirag Matai',
            'email'=>'matai@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Rahul Jethani',
            'email'=>'rahul@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Sahil dalvi',
            'email'=>'dalvi@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Soham Karalkar',
            'email'=>'soham@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Sakshi Shah',
            'email'=>'sakshi@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Mansi Pandey',
            'email'=>'mansi@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Shraddha Thakker',
            'email'=>'shraddha@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Vidhi Rughwani',
            'email'=>'vidhi@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Sakshi Awate',
            'email'=>'awate@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Shhradhha Keniya',
            'email'=>'shhradhha@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Riddhesh Shah',
            'email'=>'ridz@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Rutuja Kurhade',
            'email'=>'rutuja@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Krishna Chhabbria',
            'email'=>'krishna@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Mihir Chichria',
            'email'=>'mihir@gmail.com',
            'password'=>Hash::make('12345678')
        ]);

        User::create([
            'name'=>'Jaanvi Rijhwani',
            'email'=>'jaanvi@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        User::create([
            'name'=>'Sahil Pahuja',
            'email'=>'pahuja@gmail.com',
            'password'=>Hash::make('12345678')
        ]);

        User::create([
            'name'=>'Divesh Watwani',
            'email'=>'divesh@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Glen Dingra',
            'email'=>'glen@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Sarthak Dangalkar',
            'email'=>'sarthak@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Pranali Dharme',
            'email'=>'pranali@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Priyanka Ajwani',
            'email'=>'priyanka@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Amisha Ajwani',
            'email'=>'amisha@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Isha Jain',
            'email'=>'isha@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Dhanvi Seth',
            'email'=>'dhanvi@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Kunal Wadhwani',
            'email'=>'kunal@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Sahil Malgundkar',
            'email'=>'sahilm7@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Snehal Pohakar',
            'email'=>'snehal@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Sarwasvi Ingole',
            'email'=>'sarwasvi@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Lokesh Hinduja',
            'email'=>'lokesh@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);

        User::create([
            'name'=>'Hritik Pamnani',
            'email'=>'hritik@gmail.com',
            'password'=>Hash::make('12345678'),
        ]);
    }
}
