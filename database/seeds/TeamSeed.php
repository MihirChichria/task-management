<?php

use Illuminate\Database\Seeder;
use App\Team;
class TeamSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $team1 = Team::create([
            'name'=>'sales'
        ]);

        $team2 = Team::create([
            'name'=>'testing'
        ]);

        $team3 = Team::create([
            'name'=>'coding'
        ]);

        $team4 = Team::create([
            'name'=>'marketing'
        ]);
        $team1->members()->attach([21, 22, 23, 24, 25, 26, 27]);
        $team2->members()->attach([28, 29, 30, 31, 32]);

        $team3->members()->attach([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
        $team4->members()->attach([11, 12, 13, 14, 15, 16, 17, 18, 19, 20]);
        \App\User::find(8)->update(['role'=>'leader']);
        \App\User::find(16)->update(['role'=>'leader']);
        \App\User::find(27)->update(['role'=>'leader']);
        \App\User::find(31)->update(['role'=>'leader']);
    }
}
