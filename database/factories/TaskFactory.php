<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Task::class, function (Faker $faker) {
    return [
        'title' => trim($faker->sentence(rand(5, 8)), '.'),
        'description' => $faker->paragraphs(rand(0, 3), true),
        'priority' => rand(1, 10)
    ];
});
