$(function () {
    $("#hsn_code_form").validate({
        rules: {
            code: {
                required: true,
            },
            with_effect_from: {
                required: true,
            },
            description: {
                required: true,
            },
            cgst: {
                required: true,
                number: true
            },
            sgst: {
                required: true,
                number: true
            },
            igst: {
                required: true,
                number: true
            },
        },
        submitHandler: function (form) {
            showConfirmationSwal().then(function (result) {
                if (result.value) {
                    form.submit();
                }
            });
        }
    });
});
