var initHsnCodeDataTable = function(route, csrf_token) {
    var table = $('#hsn_code_datatable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        order: [1, 'asc'],
        ajax: {
            url: route,
            type: "POST",
            data: {
                "_token": csrf_token,
            }
        },
        columns: [
            {
                data: 'actions',
                name: 'actions',
            },
            {
                data: 'id',
                name: 'id',
            },
            {
                data: 'code',
                name: 'code',
            },
            {
                data: 'cgst',
                name: 'cgst',
            },
            {
                data: 'sgst',
                name: 'sgst',
            },
            {
                data: 'igst',
                name: 'igst',
            },
            {
                data: 'with_effect_from',
                name: 'with_effect_from',
            },
        ],
        columnDefs: [
            {
                targets: 0,
                orderable: false,
                searchable: false,
            }
        ],
    });
    // table.order( [  ] ).draw();
};

