function removeNaNChars(value) {
    return value.replace(/[^0-9]\./g, '');
}

function myFloat(obj, precision=2)
{
    let value = removeNaNChars(obj.value);
    let num = value.split('.');
    if (num[1] !== undefined) {
        obj.value = num[0] + '.' + num[1].substring(0, precision)
    }
}
