$(document).ready(function() {
    //Initialize Select2
    $('.select2').select2({
        height: '100%',
    });

    //Full width - IntlTelInput
    $('.iti').css('display', 'block');
});

var input = document.querySelector("#contact_person_number");
intlTelInput(input, {
    initialCountry: "in",
});
