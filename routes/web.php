<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::middleware(['auth'])->group(function() {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
//    =====================================TASKS======================================
    Route::get('/teams/my-teams', 'TeamsController@myIndex')->name('teams.my-team');
    Route::resource('teams', 'TeamsController');

//    =====================================TASKS======================================
    Route::put('tasks/abort/{task}', 'TasksController@abort')->name('tasks.abort');
    Route::put('tasks/decline/{task}', 'TasksController@decline')->name('tasks.decline');
    Route::put('tasks/complete/{task}', 'TasksController@complete')->name('tasks.complete');
    Route::put('tasks/resolve/{task}', 'TasksController@resolve')->name('tasks.resolve');
    Route::put('tasks/unresolve/{task}', 'TasksController@unresolve')->name('tasks.unresolve');
    Route::get('tasks/reassign/{task}', 'TasksController@reassign')->name('tasks.reassign');
    Route::post('tasks/reassign/{task}', 'TasksController@storeReassign')->name('tasks.reassign');
    Route::get('tasks/my-tasks', 'TasksController@myIndex')->name('tasks.my-tasks');
    Route::get('user-tasks/view/{user}', 'TasksController@userIndex')->name('tasks.user-tasks');
    Route::get('team-tasks/view/{team}', 'TasksController@teamIndex')->name('tasks.team-tasks');
    Route::resource('tasks', 'TasksController');
    Route::get('user-tasks/assign/{user}', 'TasksController@createWithUser')->name('tasks.createWithUser');
    Route::get('team-tasks/assign/{team}', 'TasksController@createWithTeam')->name('tasks.createWithTeam');
    Route::post('tasks/user', 'TasksController@storeAssignedTask')->name('tasks.storeAssignedTask');
    //
    Route::get('score/{user}/{year}','HomeController@score')->name('user.score');
    Route::get('updateBestMember', function () {
        \App\Team::updateBestMembers();
        return redirect()->back();
    })->name('team.updateBestMembers');
});
