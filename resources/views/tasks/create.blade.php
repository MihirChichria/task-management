@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <p class="m-0">
                @if(!((isset($team)) || (isset($user))))
                    Create Task
                @else
                    Assign Task
                @endif
            </p>
        </div>

        <div class="card-body">
            @if( (isset($user) &&  ($user->team[0]->tasks->whereNull('status')->count() == 0 )) || (isset($team) &&  ($team->tasks->whereNull('status')->count() == 0 )))
                <p>No Tasks for this team!</p>
                <a href="{{route('tasks.createWithTeam', $user->team[0])}}" class="btn btn-info text-white">Create Task</a>
            @else
                <form action="
                    @if(!((isset($team)) || (isset($user))))
                        {{ route('tasks.store') }}
                    @else
                        {{route('tasks.storeAssignedTask')}}
                    @endif
                "
                method="POST">
                @csrf

                    <!-- TITLE -->
                    @if(!((isset($team)) || (isset($user))))
                        <div class="form-group">
                            <label for="name">Title</label>
                            <input type="text"
                                   value="{{ old('title') }}"
                                   class="form-control @error('title') is-invalid @enderror"
                                   name="title" id="title">
                            @error('title')
                            <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <!-- DESCRIPTION -->
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea
                                class="form-control @error('description') is-invalid @enderror"
                                name="description" id="description">{{ old('description') }}</textarea>
                            @error('description')
                            <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                        <!-- END DESCRIPTION -->

                        <!-- PRIORITY -->
                        <div class="form-group">
                            <label for="priority">Priority</label>
                            <div>
                                <input type="range" min="1" max="10" class="d-inline range-slider @error('priority') is-invalid @enderror" value="1" step="1" name="priority">
                                <span id="range-value">1</span>
                                @error('priority')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <!-- END PRIORITY -->

                        <!-- DEADLINE -->
                        <div class="form-group">
                            <label for="deadline_at">Deadline</label>
                            <input type="text"
                                   placeholder="Select Date"
                                   value="{{ old('deadline_at') }}"
                                   class="form-control"
                                   name="deadline_at" id="deadline_at">
                            @error('deadline_at')
                            <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                        <!-- END DEADLINE -->
                    @else
                        <div class="form-group">
                            <label for="task_id">Select Task Title</label>
                            <select class="form-control" name="task_id[]" id="task_id" multiple>
                                @foreach((isset($user)) ? $user->team[0]->tasks->whereNull('status') : $team->tasks as $task)
                                    <option value="{{$task->id}}">{{$task->title}}</option>
                                @endforeach
                            </select>
                            @error('task_id')
                            <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                    @endif
                    <!-- END TITLE -->

                    @if(!((isset($team)) || (isset($user))))
                    <!-- TEAM -->
                        <div class="form-group">
                            <label for="team_id">Select Team</label>
                            <select name="team_id" id="team_id" class="form-control @error('team_id') is-invalid @enderror">
                                <option value="">Select Team</option>
                                @foreach($teams as $team)
                                    <option value="{{$team->id}}">{{\Illuminate\Support\Str::ucfirst($team->name)}}</option>
                                    @unset($team)
                                @endforeach
                            </select>
                            @error('team_id')
                            <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                    <!-- END TEAM -->
                    @endif

                    @if(isset($user))
                        <!-- MEMBERS -->
                        <div class="form-group">
                            <label for="member_id">Select Team Member</label>
                            <select name="member_id" id="member_id" class="form-control @error('member_id') is-invalid @enderror">
                                <option value="">Select Member</option>
                                @if(isset($team))
                                    @foreach ($team->members as $member)
                                        <option value="{{$member->id}}"}} {{(old('member_id') == $member->id) ? 'selected' : ''}}>{{ $member->name }}</option>
                                    @endforeach
                                @endif
                                @if(isset($user))
                                    <option selected value="{{$user->id}}">{{$user->name}}</option>
                                @endif
                            </select>
                            @error('member_id')
                            <p class="text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                        <!-- END MEMBERS -->

                            <!-- DEADLINE -->
                            <div class="form-group">
                                <label for="deadline_at">Deadline</label>
                                <input type="text"
                                       placeholder="Select Date"
                                       value="{{ old('deadline_at') }}"
                                       class="form-control"
                                       name="deadline_at" id="deadline_at">
                                @error('deadline_at')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <!-- END DEADLINE -->
                    @endif
                    @if(isset($team))
                        <p>Hello!</p>
                    @endif

                    <!-- SUBMIT BUTTON -->
                    <div class="form-group">
                        <button class="btn btn-success float-right" type="submit">
                            @if(!((isset($team)) || (isset($user))))
                                Create
                            @else
                                Assign
                            @endif
                        </button>
                        <div class="clearfix"></div>
                    </div>
                    <!-- END SUBMIT BUTTON -->

                </form>
            @endif
        </div>
    </div>
@endsection
@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr("#deadline_at", {
            enableTime: true
        });
        $('.range-slider').on('input', function () {
            console.log($('#range-value'));
            document.getElementById('range-value').innerHTML = this.value;
        });
    </script>
@endsection
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <style>
        #range-value {
            display: inline-block;
            position: relative;
            color: #fff;
            line-height: 20px;
            text-align: center;
            border-radius: 3px;
            background: #005CC8;
            padding: 5px 10px;
            margin-left: 8px;
        }
        #range-value:after {
            position: absolute;
            top: 8px;
            left: -7px;
            width: 0;
            height: 0;
            border-top: 7px solid transparent;
            border-right: 7px solid #005CC8;
            border-bottom: 7px solid transparent;
            content: '';
        }
        .range-slider{
            cursor: pointer;
        }
    </style>
@endsection
