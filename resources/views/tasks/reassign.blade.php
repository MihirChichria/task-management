@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <p class="m-0">
                Reassign Task
            </p>
        </div>

        <div class="card-body">
            <form action="{{route('tasks.reassign', $task)}}" method="POST">
                @csrf
                <!-- TITLE -->
                <div class="form-group">
                    <label for="name">Title</label>
                    <input type="text"
                           value="{{ $task->title }}"
                           class="form-control"
                            disabled>
                </div>
                <!-- END TITLE -->

                <!-- DESCRIPTION -->
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea
                        class="form-control" disabled>{{$task->description}}</textarea>
                </div>
                <!-- END DESCRIPTION -->

                <!-- DEADLINE -->
                <div class="form-group">
                    <label for="deadline_at">Deadline</label>
                    <input type="text"
                           placeholder="Select Date"
                           value="{{ $task->deadline_at }}"
                           class="form-control"
                           name="deadline_at" id="deadline_at">
                    @error('deadline_at')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <!-- END DEADLINE -->

                <!-- MEMBERS -->
                <div class="form-group">
                    <label for="member_id">Select Team Member</label>
                    <select name="member_id" id="member_id" class="form-control @error('member_id') is-invalid @enderror">
                        <option value="">Select Member</option>
                        @if(isset($team))
                            @foreach ($team->members as $member)
                                <option value="{{$member->id}}"}} {{(old('member_id') == $member->id) ? 'selected' : ''}}>{{ $member->name }}</option>
                            @endforeach
                        @endif
                        @if(isset($user))
                            <option selected value="{{$user->id}}">{{$user->name}}</option>
                        @endif
                    </select>
                    @error('member_id')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <!-- END MEMBERS -->

                <!-- SUBMIT BUTTON -->
                <div class="form-group">
                    <button class="btn btn-success float-right" type="submit">
                        Reassign
                    </button>
                    <div class="clearfix"></div>
                </div>
                <!-- END SUBMIT BUTTON -->
            </form>
        </div>
    </div>
@endsection
@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr("#deadline_at", {
            enableTime: true,
        });
        $('#deadline_at').removeAttr('readonly');
    </script>
@endsection
@section('page-level-styles')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
