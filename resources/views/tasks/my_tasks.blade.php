@extends('layouts.app')
{{--{{dd(auth()->user()->declinedTasks())}}--}}
@section('content')
    @can('assign', \App\Task::class)
        <div class="d-flex justify-content-end mb-3" xmlns="http://www.w3.org/1999/html">
            <a href="{{route('tasks.createWithUser', $user->id)}}" class="btn btn-primary">Assign Task</a>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-pills card-header-pills" id="tasks" role="tablist">
                <li class="nav-item" >
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="home" aria-selected="true">Tasks Pending</a>
                </li>
                <li class="nav-item" >
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#completed" role="tab" aria-controls="profile" aria-selected="false">Tasks Completed</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#resolved" role="tab" aria-controls="contact" aria-selected="false">Tasks Resolved</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#unresolved" role="tab" aria-controls="contact" aria-selected="false">Tasks Unresolved</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="mt-3">

                <div class="tab-content" id="myTabContent">
                    <div class="mt-4 tab-pane fade show active" id="pending" role="tabpanel" aria-labelledby="home-tab">
                        @if($user->pendingTasks()->count())
                            @foreach($user->pendingTasks as $task)
                                <div class="task-card
                                    @if(($diff = \Carbon\Carbon::parse($task->deadline_at)->diffInHours(\Carbon\Carbon::now())) <= 24)
                                    task-danger
                                    @elseif(($diff = \Carbon\Carbon::parse($task->deadline_at)->diffInDays(\Carbon\Carbon::now())) <= 3)
                                    task-warning
                                    @else
                                    task-safe
                                    @endif
                                    ">
                                    <div>
                                        <div class="float-left">
                                            <h5>{{$task->title}}</h5>
                                        </div>
                                        <div class="float-right">
                                            @if(($diff = \Carbon\Carbon::parse($task->deadline_at)->diffInHours(\Carbon\Carbon::now())) <= 24)
                                               <p class="text-danger">{{$task->deadline_at->diff(\Carbon\Carbon::now())->format('%H:%I:%S')}} Hours Remaining</p>
                                            @else
                                                <p class="text-success">{{$task->deadline_at->diffForHumans()}}</p>
                                            @endif
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <p>@if($task->description) {{\Illuminate\Support\Str::limit($task->description, 200)}} @else <p>No Description!</p>@endif</p>
                                   <div>
                                       @can('complete', $task)
                                           <a href="" class="btn btn-success"
                                              data-toggle="modal"
                                              onclick="displayModalForm({{$task}})" data-target="#completeModal">Complete</a>
                                       @endcan
                                       @can('decline', $task)
                                           <form action="{{route('tasks.decline', $task)}}" method="POST" class="d-inline">
                                               @csrf
                                               @method('PUT')
                                               <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Decline</button>
                                           </form>
                                       @endcan
                                   </div>
                                    @can('abort', $task)
                                        <div class="float-right">
                                            <form action="{{route('tasks.abort', $task)}}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <button class="btn btn-danger"
                                                    onclick="return confirm('Are you sure?')">Abort</button>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    @endcan
                                </div>
                            @endforeach
                        @else
                            <p>No Pending Tasks</p>
                        @endif

                    </div>
                    <div class="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="profile-tab">
                        @if($user->completedTasks()->count())
                            @foreach($user->completedTasks as $task)
                                <div class="task-card task-safe">
                                    <div>
                                        <div class="float-left">
                                            <h5>{{$task->title}}</h5>
                                        </div>
                                        <div class="float-right">
                                            @if($diff = \Illuminate\Support\Carbon::parse($task->updated_at)->diffInSeconds(\Illuminate\Support\Carbon::now()) <= 60)
                                                <p class="text-success">Completed Few Seconds Ago</p>
{{--                                            @elseif(($diff = \Carbon\Carbon::parse($task->updated_at)->(\Carbon\Carbon::now())) <= 1)--}}
                                            @elseif((date_format($task->updated_at, 'j') == \Illuminate\Support\Carbon::now()->day))
                                                <p class="text-success">Completed Today</p>
                                            @else
                                                <p class="text-success">Completed {{$task->updated_at->diffForHumans()}}</p>
                                            @endif
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <p><span class="text-success">Response: </span>@if($task->response) {{\Illuminate\Support\Str::limit($task->response, 200)}} @else <p>No Description!</p>@endif</p>
                                @can('resolve', $task)
                                    <form class="d-inline" action="{{route('tasks.resolve', $task)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class="btn btn-success mr-2 text-white">Resolve</button>
                                    </form>
                                @endcan
                                @can('unresolve', $task)
                                        <form class="d-inline" action="{{route('tasks.unresolve', $task)}}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <button type="submit" class="btn btn-danger text-white">Unresolve</button>
                                        </form>
                                @endcan
                                </div>
                            @endforeach
                        @else
                            <p>No Completed Tasks</p>
                        @endif
                    </div>
                    <div class="tab-pane fade" id="resolved" role="tabpanel" aria-labelledby="contact-tab">
                        @if($user->resolvedTasks()->count())
                            @foreach($user->resolvedTasks as $task)
                                <div class="task-card task-safe">
                                    <div>
                                        <div class="float-left">
                                            <h5>{{$task->title}}</h5>
                                        </div>
                                        <div class="float-right">
                                            @if($diff = \Illuminate\Support\Carbon::parse($task->updated_at)->diffInSeconds(\Illuminate\Support\Carbon::now()) <= 60)
                                                <p class="text-success">Resolved Few Seconds Ago</p>
                                            @elseif((date_format($task->updated_at, 'j') == \Illuminate\Support\Carbon::now()->day))
                                                <p class="text-success">Resolved Today</p>
                                            @else
                                                <p class="text-success">Resolved {{$task->updated_at->diffForHumans()}}</p>
                                            @endif
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <p><span class="text-success">Completed : </span>{{\Illuminate\Support\Carbon::parse($task->pivot->completed_at)->diffForHumans()}}</p>
                                    <p><span class="text-success">Response: </span>@if($task->response) {{\Illuminate\Support\Str::limit($task->response, 200)}} @else <p>No Response!</p>@endif</p>
                                </div>
                            @endforeach
                        @else
                            <p>No Resolved Tasks!</p>
                        @endif
                    </div>
                    <div class="tab-pane fade" id="unresolved" role="tabpanel" aria-labelledby="contact-tab">
                        @if($user->unresolvedTasks->count())
                            @foreach($user->unresolvedTasks as $task)
                                <div class="task-card task-danger">
                                    <div>
                                        <div class="float-left">
                                            <h5>{{$task->title}}</h5>
                                        </div>
                                        <div class="float-right">
                                            <a href="#" class="badge badge-danger p-2 font-weight-normal">{{(!$task->status == "PENDING") ? $task->status : 'DEADLINED'}}</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <p>@if($task->description) {{\Illuminate\Support\Str::limit($task->description, 200)}} @else <p>No Description!</p>@endif</p>
                                    @can('reassign', $task)
                                        <a href="{{route('tasks.reassign', $task)}}" class="btn btn-info text-white">Reassign</a>
                                    @endcan
                                </div>
                            @endforeach
                        @else
                            <p>No Unresolved Tasks</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- COMPLETE MODAL-->
    <div class="modal fade" id="completeModal" tabindex="-1" role="dialog" aria-labelledby="completeModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Complete Task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="completeForm">
                    <div class="modal-body">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <h5 id="task-title"></h5>
                        </div>
                        <div class="form-group">
                            <label for="abort_reason" class="col-form-label">Response:</label>
                            <textarea name="response" class="form-control" id="response"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Complete Task</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-level-styles')
    <style>
        .task-card {
            padding: 1.25rem;
            margin-top: 1.25rem;
            margin-bottom: 1.25rem;
            border: 1px solid #eee;
            border-left-width: .25rem;
            border-radius: .25rem;
        }
        .task-warning{
            border-left-color: #f0ad4e;
        }
        .task-danger{
            border-left-color: #dd1010;
        }
        .task-safe{
            border-left-color: #23c11d;
        }
        .task-card:hover{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
    </style>
@endsection
@section('page-level-scripts')
    <script>
        function displayModalForm(task){
            document.getElementById('task-title').innerText = task.title;
            var url = '/tasks/complete/' + task.id;
            $("#completeForm").attr('action', url);
        }
    </script>
@endsection
