@extends('layouts.app')
{{--{{dd(auth()->user()->declinedTasks())}}--}}

@section('page-level-styles')
    <style>
        .task-card {
            padding: 1.25rem;
            margin-top: 1.25rem;
            margin-bottom: 1.25rem;
            border: 1px solid #eee;
            border-left-width: .25rem;
            border-radius: .25rem;
        }
        .task-warning{
            border-left-color: #f0ad4e;
        }
        .task-danger{
            border-left-color: #dd1010;
        }
        .task-safe{
            border-left-color: #23c11d;
        }
        .task-card:hover{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
@endsection
@section('content')
    @can('assign', \App\Task::class)
        <div class="d-flex justify-content-end mb-3">
            <a href="{{route('tasks.createWithTeam', $team->id)}}" class="btn btn-primary">Assign Task</a>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-pills card-header-pills" id="tasks" role="tablist">
                <li class="nav-item" >
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="home" aria-selected="true">Tasks Pending</a>
                </li>
                <li class="nav-item" >
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#completed" role="tab" aria-controls="profile" aria-selected="false">Tasks Completed</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#resolved" role="tab" aria-controls="contact" aria-selected="false">Tasks Resolved</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#unresolved" role="tab" aria-controls="contact" aria-selected="false">Tasks Unresolved</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="mt-3">

                <div class="tab-content" id="myTabContent">
                    <div class="mt-4 tab-pane fade show active" id="pending" role="tabpanel" aria-labelledby="home-tab">
                        @if($team->tasks()->where('status', 'PENDING')->count())
                            @foreach($team->tasks->where('status', 'PENDING') as $task)
                                <div class="task-card
                                    @if(($diff = \Carbon\Carbon::parse($task->deadline_at)->diffInDays(\Carbon\Carbon::now())) <= 1)
                                    task-danger
                                    @elseif(($diff = \Carbon\Carbon::parse($task->deadline_at)->diffInDays(\Carbon\Carbon::now())) <= 3)
                                    task-warning
                                    @else
                                    task-safe
                                    @endif
                                    ">
                                    <div>
                                        <div class="float-left">
                                            <h5 id="conveying-meaning-to-assistive-technologies">{{$task->title}}</h5>
                                        </div>
                                        <div class="float-right">
                                            @if(($diff = \Carbon\Carbon::parse($task->deadline_at)->diffInDays(\Carbon\Carbon::now())) <= 1)
                                                <p class="text-danger">{{$task->deadline_at->diff(\Carbon\Carbon::now())->format('%H:%I:%S')}} Hours Remaining</p>
                                            @else
                                                <p class="text-success">{{$task->deadline_at->diffForHumans()}}</p>
                                            @endif
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <p>@if($task->description) {{\Illuminate\Support\Str::limit($task->description, 200)}} @else <p>No Description!</p>@endif</p>
                                    <div class="mt-4">
                                        <p class="float-left">Assigned To: <span class="text-success">{{$task->user[0]->name}}</span></p>
                                        @can('abort', $task)
                                            <a href="" class="btn btn-danger float-right"
                                               data-toggle="modal"
                                               onclick="displayModalForm({{$task}})" data-target="#abortModal">Abort</a>
                                        @endcan
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            @endforeach
                        @else
                            <p>No Pending Tasks</p>
                        @endif
                    </div>
                    <div class="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="profile-tab">Completed</div>
                    <div class="tab-pane fade" id="resolved" role="tabpanel" aria-labelledby="contact-tab">Resolved</div>
                    <div class="tab-pane fade" id="unresolved" role="tabpanel" aria-labelledby="contact-tab">Unresolved</div>
                </div>
            </div>
        </div>
    </div>

    <!-- ABORT MODAL -->
    <div class="modal fade" id="abortModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Abort Task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="abortForm">
                    <div class="modal-body">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="abort_reason" class="col-form-label">Aborting Reason:</label>
                            <textarea name="abort_reason" class="form-control" id="abort_reason"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Abort</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('page-level-scripts')
    <script type="text/javascript">
        function displayModalForm($task) {
            var url = '/tasks/abort/' + $task.id;
            $("#abortForm").attr('action', url);
        }
    </script>
@endsection
