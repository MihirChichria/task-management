@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header bg-white">Dashboard</div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header bg-info text-white">
                                <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                            </div>
                            <div class="card-body text-center">
                                <h3>{{count($user->pendingTasks)}}</h3>
                                <p class="p-0">Tasks Pending</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <div class="card">
                            <div class="card-header text-white" style="background-color: #ffcb74;">
                                <i class="fa fa-check fa-2x" aria-hidden="true"></i>
                            </div>
                            <div class="card-body text-center">
                                <h3>{{count($user->completedTasks)}}</h3>
                                <p class="p-0">Tasks Completed</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header text-white bg-success">
                                <i class="fa fa-trophy fa-2x" aria-hidden="true"></i>
                            </div>
                            <div class="card-body text-center">
                                <h3>{{count($user->resolvedTasks)}}</h3>
                                <p class="p-0">Tasks Resolved</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header text-white bg-danger">
                                <i class="fa fa-times fa-2x" aria-hidden="true"></i>
                            </div>
                            <div class="card-body text-center">
                                <h3>{{count($user->unresolvedTasks)}}</h3>
                                <p>Tasks Unresolved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="height: 418px;">
                    <div class="card-header">Ongoing Tasks</div>
                    <div class="card-body" style="overflow:auto;">
                        @if(count($user->pendingTasks))
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Deadline</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user->pendingTasks as $pendingTask)
                                    <tr>
                                        <td>{{$pendingTask->title}}</td>
                                        <td>{{$pendingTask->deadline_at->diffForHumans()}}</td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        @else
                            <p>No ongoing tasks!</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <p class="m-0">Tasks Ratio</p>
                    </div>
                    <canvas id="myChart" class="pb-2"></canvas>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="height: 243px;">
                    <div class="card-header">
                        Current Team: <span class="text-success">{{ucfirst(auth()->user()->team[0]->name)}}</span>
                    </div>
                    <div class="card-body" style="overflow:auto;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Team</th>
                                    <th scope="col">From</th>
                                    <th scope="col">Till</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @foreach(auth()->user()->team as $team)
                                        <td>{{ucfirst($team->name)}}</td>
                                        <td>{{date('F j, Y', strtotime($team->pivot->created_at))}}</td>
                                        <td>{{($team->pivot->deleted_at) ? date('F j, Y', strtotime($team->pivot->deleted_at)) : ''}}</td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="height: 243px;">
                    <div class="card-header">Best Members <i class="fa fa-trophy" aria-hidden="true" style="color: #cdcc00"></i>
                        <br>
                    @can('updateBestMember', auth()->user()->team[0])
                            <a href="{{route('team.updateBestMembers')}}"><i class="fa fa-refresh" style="color: #000000"></i></a>
                    @endcan
                    </div>
                    <div class="card-body" style="overflow:auto;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Month</th>
                                    <th scope="col">Year</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(auth()->user()->team[0]->getBestMembers() as $bestMember)
                                    <tr>
                                        <td>{{\App\User::find($bestMember->user_id)->name}}</td>
                                        <td>{{\Illuminate\Support\Carbon::parse($bestMember->date)->monthName}}</td>
                                        <td>{{\Illuminate\Support\Carbon::parse($bestMember->date)->year}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <canvas id="score-chart" height="350"></canvas>
        </div>
    </div>
</div>
@endsection
@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script>
        var ctx1 = document.getElementById('myChart').getContext('2d');
        var doughnut =  new Chart(ctx1,
        {
            type:"doughnut",
            data: {
                    labels:["Tasks Resolved", "Tasks Unresolved"],
                    datasets: [
                            {
                                label:"Performance Score",
                                backgroundColor:["rgb(56, 193, 114)", "rgb(227, 52, 47)"]
                            }
                        ],
                }
        });
        var temp = [];
        temp.push({!! count(auth()->user()->resolvedTasks) !!});
        temp.push({!! count(auth()->user()->unresolvedTasks) !!});
        doughnut.config.data.datasets[0].data = temp;
        doughnut.update();
        var ctx2 = document.getElementById('score-chart').getContext('2d');
        var chart = new Chart(ctx2,{
           type: 'line',
           data: {
               labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
               datasets: [
                   {
                       label: 'Performance Scores'
                   }
               ],
           },
            options: {
                maintainAspectRatio: false,
               responsive: true,
                scales: {
                    yAxes: [{
                        display: true,
                        gridLines: {
                            color: "rgb(210,210,211)"
                        },
                        ticks: {
                            stepSize: 10,
                            beginAtZero: true,
                            padding: 20,
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0
                    }
                }
            }

        });
        let scores=[], labels=[];
        $.ajax({
            url: '{{url('score/'. auth()->user()->id ."/". \Illuminate\Support\Carbon::now()->year)}}',
            type: 'GET',
            success:function(data) {
                data.forEach(function (item) {
                    scores.push(item['score']);
                    labels.push(item['month']);
                });
                chart.config.data.datasets[0].data = scores;
                chart.config.data.labels = labels;
                chart.update();
            }
        });
        // ctx2.height = 500;
    </script>
@endsection
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.css">
@endsection
