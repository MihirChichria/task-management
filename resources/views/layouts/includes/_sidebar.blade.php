{{--<ul class="list-group">--}}
{{--    <li class="list-group-item">--}}
{{--        <a href="">Users</a>--}}
{{--    </li>--}}
{{--    <li class="list-group-item">--}}
{{--        <a href="{{route('teams.index')}}">Teams</a>--}}
{{--    </li>--}}
{{--    <li class="list-group-item">--}}
{{--        <a href="{{route('tasks.index')}}">Tasks</a>--}}
{{--    </li>--}}
{{--</ul>--}}
{{--<ul class="list-group mt-3">--}}
{{--    <li class="list-group-item">--}}
{{--        <a href=""></a>--}}
{{--    </li>--}}
{{--</ul>--}}
    <!-- Sidebar -->
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
        <!-- d-* hiddens the Sidebar in smaller devices. Its itens can be kept on the Navbar 'Menu' -->
        <!-- Bootstrap List Group -->
        <ul class="list-group">

            <a href="#" class="bg-white list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-users fa-fw mr-3"></span>
                    <span class="menu-collapsed">Users</span>
                    <span class="submenu-icon ml-auto"></span>
                </div>
            </a>

            <a href="#teams-menu" data-toggle="collapse" aria-expanded="false" class="bg-white list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-users fa-fw mr-3"></span>
                    <span class="menu-collapsed">Teams</span>
                    <span class="submenu-icon ml-auto"></span>
                </div>
            </a>

            <div id='teams-menu' class="collapse sidebar-submenu">
                @can('viewAllTeams', \App\Team::class)
                    <a href="{{route('teams.index')}}" class="list-group-item list-group-item-action bg-white">
                        <span class="menu-collapsed">All Teams</span>
                    </a>
                @endcan
                <a href="{{route('teams.my-team')}}" class="list-group-item list-group-item-action bg-white">
                    <span class="menu-collapsed">My Teams</span>
                </a>
            </div>

            <!-- Menu with submenu -->
            <a href="#tasks-menu" data-toggle="collapse" aria-expanded="false" class="bg-white list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-dashboard fa-fw mr-3"></span>
                    <span class="menu-collapsed">Tasks</span>
                    <span class="submenu-icon ml-auto"></span>
                </div>
            </a>
            <!-- Submenu content -->

            <div id='tasks-menu' class="collapse sidebar-submenu">
                @can('create', \App\Task::class)
                    <a href="{{route('tasks.create')}}" class="list-group-item list-group-item-action bg-white">
                        <span class="menu-collapsed">Create Task</span>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action bg-white">
                        <span class="menu-collapsed">View Unassigned Tasks</span>
                    </a>
                @endcan
                <a href="{{route('tasks.my-tasks')}}" class="list-group-item list-group-item-action bg-white">
                    <span class="menu-collapsed">My Tasks</span>
                </a>
            </div>
            <a href="#submenu2" data-toggle="collapse" aria-expanded="false" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-user fa-fw mr-3"></span>
                    <span class="menu-collapsed">Profile</span>
                    <span class="submenu-icon ml-auto"></span>
                </div>
            </a>
            <!-- Submenu content -->
            <div id='submenu2' class="collapse sidebar-submenu">
                <a href="#" class="list-group-item list-group-item-action bg-white">
                    <span class="menu-collapsed">Settings</span>
                </a>
                <a href="#" class="list-group-item list-group-item-action bg-white">
                    <span class="menu-collapsed">Password</span>
                </a>
            </div>
        </ul>
    </div>
<style>

    /* Closed submenu icon */
    #sidebar-container .list-group .list-group-item[aria-expanded="false"] .submenu-icon::after {
        content: " \f0d7";
        font-family: FontAwesome;
    }
    /* Opened submenu icon */
    #sidebar-container .list-group .list-group-item[aria-expanded="true"] .submenu-icon::after {
        content: " \f0da";
        font-family: FontAwesome;
    }
</style>
