@extends('layouts.app')
{{--{{dd($data[0][0])}}--}}
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 text-primary">View Team</h6>
        </div>
        <div class="card-body">
            <div class="">
                <div class="float-left">
                    <p class="text-primary d-inline-block">Name of Team: </p>
                    <p class="d-inline-block">{{\Illuminate\Support\Str::ucfirst($team->name)}}</p>
                </div>
                @can('viewAny', \App\Task::class)
                    <div class="float-right">
                        <a href="{{route('tasks.team-tasks', $team)}}" class="btn-dark btn btn-sm text-white">View Team Tasks Status</a>
                    </div>
                @endcan
                <div class="clearfix"></div>
            </div>
            <div class="">
                <p class="text-primary d-inline-block">Name of Team Leader: </p>
                <p class="d-inline-block">{{\Illuminate\Support\Str::ucfirst($team->leader[0]['name'])}}</p>
            </div>
            <div class="table-responsive table-striped">
                <table class="table table-hover table-bordered" id="members-datatable" width="100%" cellspacing="0">

                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Tasks</th>
                            <th width="30%">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->tasks()->count()}}</td>
                                <td>
                                    @can('assign', \App\Task::class)
                                        <a href="{{route('tasks.createWithUser', $user->id)}}" class="btn btn-info btn-sm text-white">Assign Task</a>
                                    @endcan
                                    @can('viewAny', \App\Task::class)
                                        <a href="{{route('tasks.user-tasks', $user)}}" class="btn-dark btn btn-sm text-white">View Tasks</a>
                                    @else
                                        <p>No Actions</p>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <script>
        $('#members-datatable').dataTable({
            "order": [
                [1, "ASC"]
            ],
            "columnDefs": [{
                'orderable': false,
                'targets': [-1]
            }]
        });
    </script>
@endsection
@section('page-level-styles')
    <link rel="stylesheet" href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}">
@endsection
