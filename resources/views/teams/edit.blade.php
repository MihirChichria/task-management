@extends('layouts.app')

@section('page-level-styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <p class="m-0">Update Team</p>
        </div>

        <div class="card-body">
            <form action="{{ route('teams.store') }}" method="POST">
            @csrf

            <!-- NAME -->
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text"
                           value="{{ old('name', $team->name) }}"
                           class="form-control @error('name') is-invalid @enderror"
                           name="name" id="name">
                    @error('name')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <!-- END NAME -->
                <div class="row">
                    <div class="col-md-6">
                    <!-- TEAM MEMBERS -->
                    <div class="form-group">
                        <label for="member_id">Select Team Members</label>
                        <select name="member_id[]" id="member_id" class="form-control" multiple="multiple">
                            @foreach ($members as $member)
                                <option value="{{$member->id}}" selected>{{ $member->name }}</option>
                            @endforeach
                            @foreach ($freeMembers as $freeMember)
                                <option value="{{$freeMember->id}}" @if(old('member_id')){{ in_array($freeMember->id, old('member_id')) ? 'selected' : '' }} @endif>{{ $freeMember->name }}</option>
                            @endforeach
                        </select>
                        @error('member_id')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <!-- END TEAM MEMBERS -->
                    </div>
                    <div class="col-md-6">
                    <!-- TEAM LEADER -->
                    <div class="form-group">
                        <label for="leader_id">Select Team Leader</label>
                        <select name="leader_id" id="leader_id" class="form-control">
                            <option value="">Select Team Leader</option>
                        </select>
                        @error('leader_id')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <!-- END TEAM LEADER -->
                    </div>
                </div>
                <!-- SUBMIT BUTTON -->
                <div class="form-group">
                    <button class="btn btn-success float-right" type="submit">Create Team</button>
                    <div class="clearfix"></div>
                </div>
                <!-- END SUBMIT BUTTON -->

            </form>
        </div>
    </div>
@endsection
@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        if(document.getElementById('member_id').options.length){
            updateLeaderSelect();
        }
        $('#member_id').select2({
            placeholder: "Select members"
        });
        $('#member_id').change(function (){
            updateLeaderSelect();
        });
        function updateLeaderSelect(){
            var txtSelectedValuesObj = document.getElementById('leader_id');
            var selectedArrayTexts = new Array();
            var selectedArrayValues = new Array();
            var selObj = document.getElementById('member_id');
            var i;
            var count = 0;
            for (i=0; i<selObj.options.length; i++) {
                if (selObj.options[i].selected) {
                    selectedArrayTexts[count] = selObj.options[i].value;
                    selectedArrayValues[count] = selObj.options[i].text;
                    count++;
                }
            }
            document.getElementById("leader_id").innerHTML = "<option value='0' selected>Select Team Leader</option>";
            for(i=0; i<selectedArrayValues.length; i++){
                document.getElementById("leader_id").innerHTML += `<option value='${selectedArrayTexts[i]}'> ${selectedArrayValues[i]}</option>`;
            }
        }
    </script>
@endsection
