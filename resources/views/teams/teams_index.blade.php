@extends('layouts.app')

@section('content')

    <div class="card shadow-sm">
        <div class="card-header">
            <div class="d-flex justify-content-between">
                <h4>All Teams</h4>
                @can('create', \App\Team::class)
                <div>
                    <a href="{{ route('teams.create') }}" class="btn btn-primary">Create Team</a>
                </div>
                @endcan
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                <th>Name</th>
                <th>Members Count</th>
                <th>Actions</th>
                </thead>
                <tbody>
                @foreach($teams as $team)
                    <tr>
                        <td>
                            {{\Illuminate\Support\Str::ucfirst($team->name)}}
                        </td>
                        <td>
                            {{$team->members()->count()}}
                        </td>
                        <td>
                            <a href="{{route("teams.show", $team)}}" class="btn btn-sm btn-success"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            @can('update', $team)
                                <a href="{{ route('teams.edit', $team) }}" class="btn btn-warning btn-sm"><i class="fa fa-pen" aria-hidden="true"></i></a>
                            @endcan
                            @can('delete', $team)
                                <a href="" class="btn btn-danger btn-sm"
                                   data-toggle="modal"
                                   onclick="displayModalForm({{$team}})" data-target="#deleteModal"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- DELETE MODAL -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" METHOD="POST" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Are you sure you want to delete this Team?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete Team</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script type="text/javascript">
        function displayModalForm($team) {
            var url = '/teams/' + $team.id;
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection
