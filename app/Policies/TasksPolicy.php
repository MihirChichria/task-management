<?php

namespace App\Policies;

use App\Task;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Carbon;

class TasksPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        return $user->role == "manager" || $user->role == "leader";
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function view(User $user, Task $task)
    {

    }

    public function create(User $user){
        return $user->role == "leader" || $user->role == "manager";
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function assign(User $user)
    {
        //
        return $user->role == "manager" || $user->role == "leader";
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function update(User $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function delete(User $user, Task $task)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function restore(User $user, Task $task)
    {
        //
    }

    public function abort(User $user, Task $task){
        return ($task->team->title == $user->team[0]->title && $user->role == "leader") || $user->role == "manager";
    }

    public function decline(User $user, Task $task){
        return $task->user[0]->name == $user->name && $task->deadline_at >= Carbon::now();
    }

    public function complete(User $user, Task $task){
        return $task->user[0]->name == $user->name && $task->deadline_at >= Carbon::now();
    }
    public function resolve(User $user, Task $task){
        return $task->team->title == $user->team[0]->title && $user->role == "leader" || $user->role == "manager";
    }
    public function unresolve(User $user, Task $task){
        return $task->team->title == $user->team[0]->title && $user->role == "leader" || $user->role == "manager";
    }
    public function reassign(User $user, Task $task){
        return ($task->team->title == $user->team[0]->title && $user->role == "leader" || $user->role == "manager") && ($task->status = "DECLINED");
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function forceDelete(User $user, Task $task)
    {
        //
    }
}
