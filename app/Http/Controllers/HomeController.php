<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();
        return view('home', compact('user'));
    }
    public function score($user, $year){
        $sql = "select task_user.user_id, MONTHNAME(tasks.created_at) as month, SUM((((TIMESTAMPDIFF(MINUTE, tasks.created_at, tasks.deadline_at) + ((TIMESTAMPDIFF(MINUTE, task_user.completed_at, tasks.deadline_at)*40)/100))/10)*(tasks.priority*30)/100)/100) as score from tasks, task_user WHERE tasks.status='RESOLVED' AND task_user.task_id = tasks.id AND task_user.status='RESOLVED' AND task_user.user_id=$user AND YEAR(tasks.created_at) = $year GROUP BY MONTH(tasks.created_at)";
        return response()->json(DB::select(DB::raw($sql)));
    }
}
