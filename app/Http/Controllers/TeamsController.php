<?php

namespace App\Http\Controllers;

use App\Http\Requests\Teams\CreateTeamRequest;
use App\Http\Requests\Teams\UpdateTeamRequest;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamsController extends Controller
{
    public function index()
    {
        $this->authorize('viewAllTeams', Team::class);
        $teams = Team::all();
            return view('teams.teams_index', compact([
                'teams'
            ]));
    }
    public function myIndex(){
        $team = auth()->user()->team[0];
        return view('teams.my_index', compact([
            'team'
        ]));
    }

    public function view(Team $team){

    }

    public function create()
    {
        $this->authorize('create', Team::class);
        $freeMembers = User::select('id', 'name')->whereNotIn('id', function ($query){
            $query->select('user_id')->from('team_user');
        })->get();
        return view('teams.create', compact([
            'freeMembers'
        ]));
    }

    public function store(CreateTeamRequest $request)
    {
        $this->authorize('create', Team::class);
        try {
            Team::persistTeam($request->toArray());
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception->getMessage());
        }
        return redirect()->route('teams.index')->with('success', 'Team created successfully');
    }

    public function show(Team $team)
    {
        $users = $team->members;
        return view('teams.view', compact([
            'team',
            'users'
        ]));
    }

    public function edit(Team $team)
    {
        $this->authorize('update', [$team]);
        $freeMembers = User::select('id', 'name')->whereNotIn('id', function ($query){
            $query->select('user_id')->from('team_user');
        })->get();
        $members = $team->members;
        return view('teams.edit', compact([
            'team',
            'members',
            'freeMembers'
        ]));
    }

    public function update(UpdateTeamRequest $request, Team $team)
    {
        $this->authorize('update', Team::class);
        try {
            $team->updateTeam($request->toArray());
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception->getMessage());
        }
        return redirect()->route('teams.index')->with('success', 'Team created successfully');
    }

    public function destroy(Team $team)
    {

    }
}
