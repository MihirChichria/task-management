<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tasks\CreateTaskRequest;
use App\Http\Requests\Tasks\ReassignTaskRequest;
use App\Http\Requests\Teams\CreateTeamRequest;
use App\Task;
use App\Team;
use App\User;
use http\Env\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }
    public function myIndex(){
        $user = auth()->user();
        return view('tasks.my_tasks', compact('user'));
    }
    public function userIndex(User $user){
        $this->authorize('viewAny', Task::class);
        return view('tasks.my_tasks', compact('user'));
    }
    public function teamIndex(Team $team){
        $this->authorize('view', $team);
        return view('tasks.teams_tasks', compact('team'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::all();
        return view('tasks.create', compact([
            'teams'
        ]));
    }
    public function createWithUser(User $user){
        return view('tasks.create', compact([
            'user'
        ]));
    }
    public function createWithTeam(Team $team){
        return view('tasks.create', compact([
            'team'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTaskRequest $request)
    {
        //
        $task = Task::create([
            'title'=>$request->title,
            'description'=>$request->description,
            'created_by'=>auth()->id(),
            'priority'=>$request->priority,
            'team_id'=>$request->team_id
        ]);

        return redirect(route('teams.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
    public function storeAssignedTask(CreateTaskRequest $request){
        $user = User::find($request->member_id);
        $user->tasks()->attach($request->task_id, ['status'=>'PENDING']);
        foreach ($request->task_id as $task){
            $task = Task::find($task);
            $task->status = 'PENDING';
            if(isset($request->deadline_at)){
                $task->deadline_at = $request->deadline_at;
            }
            $task->save();
        }
        //redirect to dashboard
        return redirect(route('teams.index'));
    }
    public function complete(Request $request, Task $task){
        $task->status = "COMPLETED";
        $task->response = $request->response;
        $task->save();
        DB::table('task_user')->where('task_id', '=', $task->id)->update(['status'=>'COMPLETED', 'completed_at'=>Carbon::now()]);
        return redirect()->back();
    }
    public function abort(Request $request, Task $task){
        $this->authorize('abort', $task);
        $task->user[0]->tasks()->updateExistingPivot($task->id, ['status'=>'RELINQUISHED']);
        $task->response = $request->abort_reason;
        $task->status = "ABORTED";
        $task->save();
        return redirect()->back();
    }
    public function decline(Task $task){
        $task->status = "DECLINED";
        $task->save();
        $task->user[0]->tasks()->updateExistingPivot($task->id, ['status'=>'DECLINED']);
        return redirect()->back();
    }
    public function resolve(Task $task){
        $task->status = "RESOLVED";
        $task->save();
        $task->user[0]->tasks()->updateExistingPivot($task->id, ['status'=>'RESOLVED']);
        return redirect()->back();
    }
    public function unresolve(Task $task){
        $task->status = "REJECTED";
        $task->save();
        $task->user[0]->tasks()->updateExistingPivot($task->id, ['status'=>'REJECTED']);
        return redirect()->back();
    }
    public function reassign(Task $task){
        $team = Team::find($task->team_id);
        return view('tasks.reassign', compact([
            'team', 'task'
        ]));
    }
    public function storeReassign(ReassignTaskRequest $request, Task $task){
        $task->status = "PENDING";
        $task->save();
        $user = User::find($request->member_id);
        $user->tasks()->attach($task->id, ['status'=>'PENDING']);
        return redirect('home');
    }
}
