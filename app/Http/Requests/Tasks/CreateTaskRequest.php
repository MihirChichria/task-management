<?php

namespace App\Http\Requests\Tasks;

use App\Task;
use Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class CreateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function validatePrevDeadline($validator){
        $tasks = $this->only(['task_id']);
        $deadline = $this->only(['deadline_at'])['deadline_at'];
        /*
         * This rule is to check that task deadline is filled prev! If not user have to fill!
         */
        foreach ($tasks as $task){
            if(!isset(Task::find($task)[0]->deadline_at) && !isset($deadline)){
                return false;
            }
            return true;
        }
    }
    public function rules()
    {
        Validator::extend('validate_deadline', function($attribute, $value, $parameters, $validator)
        {
//            dd($validator);
            $tasks = $this->only(['task_id']);
            /*
             * This rule is to check that time is greater than now!
             */

            if (!$this->validatePrevDeadline($validator)){
                $validator->addReplacer('validate_deadline',function($message, $attribute, $rule, $parameters) {
                    return \str_replace(':message', "Deadline is required!", $message);
                });
                return false;
            }
            if($value < \Illuminate\Support\Carbon::now() && count($tasks) ){
                if ($this->validatePrevDeadline($validator)){
                    return true;
                }
                $validator->addReplacer('validate_deadline', function ($message, $attribute, $rule, $parameters) {
                    return \str_replace(':message', "Deadline should not be less than " . \Illuminate\Support\Carbon::now() . "!", $message);
                });
                return false;
            }
            return true;
        }, ':message');
        switch (Request::route()->getName()) {
            case 'tasks.store':
            {
                return [
                    'title' => 'required',
                    'priority' => 'required|integer|min:1|max:10',
                    'team_id'=>'required|exists:teams,id',
                    'deadline_at'=>'date|after:' . date('Y-m-d h:i:s')
                ];
            }
            case 'tasks.storeAssignedTask':
            {
                return [
                    'task_id' => 'required|exists:tasks,id',
                    'member_id' => 'required|integer|exists:users,id|min:1',
                    'deadline_at' => 'validate_deadline'
                ];
            }

        }
    }
}
