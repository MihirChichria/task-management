<?php

namespace App\Http\Requests\Teams;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class CreateTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   $members = array();
        if(Request::only('member_id')) {
            $members = Request::only('member_id')['member_id'];
        }
        return [
            'name'=>'required|unique:teams',
            'member_id'=>'required|exists:users,id|array',
            'leader_id'=> 'required|exists:users,id|integer|in:' . implode(',', $members)
        ];
    }
}
