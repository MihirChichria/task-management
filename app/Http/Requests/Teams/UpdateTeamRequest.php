<?php

namespace App\Http\Requests\Teams;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class UpdateTeamRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {   $members = array();
        if(Request::only('member_id')) {
            $members = Request::only('member_id')['member_id'];
        }
        return [
            'name'=>'required|unique:teams,'.$this->team,
            'member_id'=>'required|exists:users,id|array',
            'leader_id'=> 'required|exists:users,id|integer|in:' . implode(',', $members)
        ];
    }
}
