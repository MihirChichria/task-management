<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $fillable = ['title', 'deadline_at', 'created_by', 'priority', 'team_id'];
    protected $dates = ['deadline_at', 'completed_at'];


    /*
     * RELATIONSHIP METHODS
     */
    public function team(){
        return $this->belongsTo(Team::class);
    }
    public function user(){
        return $this->belongsToMany(User::class);
    }
}
