<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Team extends BaseModel
{

    /*
     * RELATIONSHIP METHODS
     */
    public function members(){
        return $this->belongsToMany(User::class)->whereNull('team_user.deleted_at')->withTimestamps();
    }
    public function tasks(){
        return $this->hasMany(Task::class);
    }

    public function leader(){
        return $this->members()->where('role', '=', 'leader');
    }

    public static function persistTeam(array $data): Team
    {
        $team = null;
        DB::transaction(function () use (&$team, $data) {
            $team = Team::create([
                'name'=>$data['name']
            ]);
            $team->members()->attach($data['member_id']);
            $user = User::find($data['leader_id']);
            $user->role = "leader";
            $user->save();
        });
        return $team;
    }

    public function updateTeam(array $data): Team
    {
        DB::transaction(function () use (&$team, $data) {
            $this->update([
                'name'=>$data['name']
            ]);
            $team->members()->sync($data['member_id']);
            $user = User::find($data['leader_id']);
            $user->role = "leader";
            $user->save();
        });
        return $team;
    }

    public static function updateBestMembers(){
        $teams = Team::select('id')->pluck('id')->toArray();
        $maxDate = \Carbon\Carbon::make(DB::table('tasks')->max('created_at'));
//        dd($maxDate);
        foreach ($teams as $team_id) {
            $lastDateAwarded = \Carbon\Carbon::make(DB::table('best_members')->select('date')->where('team_id', $team_id)->orderByDesc('date')->limit(1)->first('date'));
            if (!$lastDateAwarded) {
                //There are no best members calculate first month and start filling best members
                $minDate = \Carbon\Carbon::make(DB::table('tasks')->min('created_at'));
                while ($maxDate->month - $minDate->month >= 1){
                    var_dump($maxDate->month - $minDate->month);
                    $month = $minDate->month;
                    $bestMemberId = self::getBestMember($team_id, $minDate->year, $minDate->month);

                    if ($bestMemberId){
                        $result = DB::table('best_members')->insert([
                            'user_id' => $bestMemberId,
                            'team_id' => $team_id,
                            'date' => $minDate->lastOfMonth()
                        ]);
                    }
                    $minDate->firstOfMonth();
                    $minDate->addMonth();
                }
            } else {
                $lastDateAwarded->firstOfMonth();
                $lastDateAwarded->addMonth();
                while ($maxDate->month - $lastDateAwarded->month > 1){
                    $month = $lastDateAwarded->month;
                    $bestMemberId = self::getBestMember($team_id, $lastDateAwarded->year, $lastDateAwarded->month);
                    if ($bestMemberId){
                        DB::table('best_members')->insert([
                            'user_id' => $bestMemberId,
                            'team_id' => $team_id,
                            'date' => $lastDateAwarded->lastOfMonth()
                        ]);
                    }
                    $lastDateAwarded->firstOfMonth();
                    $lastDateAwarded->addMonth();
                }
            }
        }
    }

    public static function getBestMember($team_id, $year, $month){
        $preExecutableSql = "SELECT COUNT(tasks.id) as tasksCount FROM tasks WHERE MONTH(tasks.created_at) = '$month' AND YEAR(tasks.created_at) = '$year'";
        if(DB::select(DB::raw($preExecutableSql))[0]->tasksCount) {
            $sql = "select task_user.user_id, SUM((((TIMESTAMPDIFF(MINUTE, tasks.created_at, tasks.deadline_at) + ((TIMESTAMPDIFF(MINUTE, task_user.completed_at, tasks.deadline_at)*40)/100))/10)*(tasks.priority*30)/100)/100) as score from tasks, task_user, team_user WHERE tasks.status='RESOLVED' AND task_user.task_id = tasks.id AND task_user.status='RESOLVED' AND task_user.user_id = team_user.user_id AND team_user.team_id= '$team_id' AND MONTH(tasks.created_at) = '$month' AND YEAR(tasks.created_at) = '$year' GROUP BY task_user.user_id ORDER BY score DESC LIMIT 1";
            return DB::select(DB::raw($sql))[0]->user_id;
        }else{
            return  0;
        }
    }
    public function getBestMembers(){
        $team_id = auth()->user()->team[0]->id;
        return DB::select(DB::raw("SELECT * FROM best_members, team_user WHERE best_members.user_id = team_user.user_id AND team_user.team_id = ".$team_id));
    }

    public static function getCreateValidationRules(string $nameExtension = '', bool $withChildRules = true): array
    {
        // TODO: Implement getCreateValidationRules() method.
    }

    public static function getUpdateValidationRules(string $nameExtension = '', int $id = 0, bool $withChildRules = true): array
    {
        // TODO: Implement getUpdateValidationRules() method.
    }
}
