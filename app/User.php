<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * ACCESSORS
     */
    /**
     * RELATIONSHIP METHODS
     */
    public function team() {
//        return $this->belongsToMany(Team::class)->whereNull('team_user.deleted_at')->withTimestamps();
        return $this->belongsToMany(Team::class)->withPivot('deleted_at')->withTimestamps()->orderByDesc('created_at');
    }
    public function tasks(){
        return $this->belongsToMany(Task::class)->withTimestamps();
    }
    /**
     * HELPER METHODS
     */
    public function pendingTasks(){
        return $this->tasks()->where('tasks.status', 'PENDING')->whereDate('deadline_at', '>=', Carbon::now());
    }
    public function completedTasks(){
        return $this->tasks()->where('task_user.status', 'COMPLETED');
    }
    public function resolvedTasks(){
        return $this->tasks()->where('tasks.status', 'RESOLVED')->withPivot('completed_at')->withTimestamps();
    }
    public function declinedTasks(){
        return $this->tasks()->where('task_user.status', 'DECLINED');
    }
    public function deadlinedTasks(){
        return $this->tasks()->where('tasks.status', '=', 'PENDING')->where('tasks.deadline_at', '<', Carbon::now());
    }
    public function relinquishedTasks(){
        return $this->tasks()->where('task_user.status', 'RELINQUISHED');
    }
    public function unresolvedTasks(){
        return $this->tasks()->where(function ($q){
            $q->where('task_user.status', 'DECLINED')
            ->orWhere('task_user.status', 'RELINQUISHED')
            ->orWhere('task_user.status', 'REJECTED')
            ->orWhere('tasks.status', 'PENDING')
            ->where('tasks.deadline_at', '<', Carbon::now());
        });
    }
}
