<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    protected $guarded = ['id'];
    public $timestamps = true;

    /**
     * Shouldn't ever be used, but this language allows it soo...
     * @return array
     */
    public abstract static function getCreateValidationRules(string $nameExtension = '', bool $withChildRules=true) : array;
    public abstract static function getUpdateValidationRules(string $nameExtension = '', int $id=0, bool $withChildRules=true) : array;
}
